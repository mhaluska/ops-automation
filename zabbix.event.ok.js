const config = require('./includes/config.js');
const amqp = require('./includes/rabbitmq.js');
const pool = require('./includes/db.js').pool;
const log4js = require('log4js').configure(config.log);

const queuein = 'zabbix.event.ok';
const queueout = 'otrs.ticket.resolve';

const log = log4js.getLogger(queuein);
const channelWrapper = amqp.channelWrapper;

// Main processing function
const processData = async (data, retry) => {
  const msg = JSON.parse(data.content);
  try {
    log.info('Got new message. EventID %d (%d/%d)', msg.eventid, retry, config.amqp.retrynum);
    const res = await pool.query(`UPDATE ${config.db.tbevt} SET recoveryid = ?, recoverytime = ? WHERE eventid = ?`, [msg.recoveryid, msg.eventtime, msg.eventid]);
    if (res.affectedRows !== 1) {
      await channelWrapper.nack(data, false, false);
      log.warn('Failed to process message! EventID %d (%d/%d) Error: EventID not found in database', msg.eventid, retry, config.amqp.retrynum);
    } else {
      const [row] = await pool.query(`SELECT ticketid FROM ${config.db.tbevt} WHERE eventid = ?`, [msg.eventid]);
      if (row.ticketid && row.ticketid !== null) {
        msg.ticketid = row.ticketid;
        await channelWrapper.publish(config.amqp.exinput, queueout, new Buffer.from(JSON.stringify(msg)), config.amqp.msgopt);
        await channelWrapper.ack(data);
        log.info('Processed message. EventID %d TicketID %d (%d/%d)', msg.eventid, msg.ticketid, retry, config.amqp.retrynum);
      } else {
        await channelWrapper.nack(data, false, false);
        log.warn('Failed to process message! EventID %d (%d/%d) Error: TicketID not found in database', msg.eventid, retry, config.amqp.retrynum);
      }
    }
  } catch (err) {
    await channelWrapper.nack(data, false, false);
    log.warn('Failed to process message! EventID %d (%d/%d) Error:', msg.eventid, retry, config.amqp.retrynum, err.message);
  }
}

// Setup AMQP loggin
amqp.logging(queuein);
// Start message processing
amqp.consume(processData, queuein, queueout);
