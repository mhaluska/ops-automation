const config = require('./includes/config.js');
const amqp = require('./includes/rabbitmq.js');
const pool = require('./includes/db.js').pool;
const otrs = require('./includes/otrs.js');
const log4js = require('log4js').configure(config.log);

const queuein = 'otrs.ticket.resolve';

const log = log4js.getLogger(queuein);
const channelWrapper = amqp.channelWrapper;

// Main processing function
const processData = async (data, retry) => {
  const msg = JSON.parse(data.content);
  try {
    log.info('Got new message. EventID %d (%d/%d)', msg.eventid, retry, config.amqp.retrynum);
    const body = new Buffer.from(msg.message, 'base64').toString('utf8');
    const [row] = await pool.query(`SELECT execstatus FROM ${config.db.tbevt} WHERE eventid = ?`, [msg.eventid]);

    if (row.execstatus && row.execstatus == 'succeeded') {

      const res = await otrs.updateTicket(msg.ticketid, 'closed successful', 'Automation resolve ticket', body);
      if (res == 'OK') {
        await channelWrapper.ack(data);
        log.info('Processed message. EventID %d TicketID %d (%d/%d)', msg.eventid, msg.ticketid, retry, config.amqp.retrynum);
      } else {
        await channelWrapper.nack(data, false, false);
        log.warn('Failed to process message! EventID %d (%d/%d)', msg.eventid, retry, config.amqp.retrynum, res);
      }

    } else if (row.execstatus && row.execstatus == 'started') {
      if (retry == config.amqp.retrynum) {
        const res = await otrs.updateTicket(msg.ticketid, 'open', 'Automation update ticket', body);
        if (res == 'OK') {
          await channelWrapper.ack(data);
          log.info('Processed message. EventID %d TicketID %d (%d/%d)', msg.eventid, msg.ticketid, retry, config.amqp.retrynum);
        } else {
          await channelWrapper.nack(data, false, false);
          log.warn('Failed to process message! EventID %d (%d/%d)', msg.eventid, retry, config.amqp.retrynum, res);
        }
      } else {
        await channelWrapper.nack(data, false, false);
        log.warn('Failed to process message! EventID %d (%d/%d) Warning: execution is still running', msg.eventid, retry, config.amqp.retrynum);
      }

    } else if (row.execstatus) {
      const res = await otrs.updateTicket(msg.ticketid, 'open', 'Automation update ticket', body);
      if (res == 'OK') {
        await channelWrapper.ack(data);
        log.info('Processed message. EventID %d TicketID %d (%d/%d)', msg.eventid, msg.ticketid, retry, config.amqp.retrynum);
      } else {
        await channelWrapper.nack(data, false, false);
        log.warn('Failed to process message! EventID %d (%d/%d)', msg.eventid, retry, config.amqp.retrynum, res);
      }

    } else {
      if (retry == config.amqp.retrynum) {
        const res = await otrs.updateTicket(msg.ticketid, 'open', 'Automation update ticket', body);
        if (res == 'OK') {
          await channelWrapper.ack(data);
          log.info('Processed message. EventID %d TicketID %d (%d/%d)', msg.eventid, msg.ticketid, retry, config.amqp.retrynum);
        } else {
          await channelWrapper.nack(data, false, false);
          log.warn('Failed to process message! EventID %d (%d/%d)', msg.eventid, retry, config.amqp.retrynum, res);
        }
      } else {
        await channelWrapper.nack(data, false, false);
        log.warn('Failed to process message! EventID %d (%d/%d) Error:', msg.eventid, retry, config.amqp.retrynum, row);
      }
    }

  } catch (err) {
    await channelWrapper.nack(data, false, false);
    log.warn('Failed to process message! EventID %d (%d/%d) Error:', msg.eventid, retry, config.amqp.retrynum, err.message);
  }
}

// Setup AMQP logging
amqp.logging(queuein);
// Start message processing
amqp.consume(processData, queuein);
