const config = require('./includes/config.js');
const amqp = require('./includes/rabbitmq.js');
const pool = require('./includes/db.js').pool;
const rd = require('./includes/rundeck.js');
const log4js = require('log4js').configure(config.log);

const queuein = 'rundeck.exec.watch';
const queueout = 'otrs.ticket.update';

const log = log4js.getLogger(queuein);
const channelWrapper = amqp.channelWrapper;

// Main processing function
const processData = async (data, retry) => {
  const msg = JSON.parse(data.content);
  try {
    log.info('Got new message. EventID %d (%d/%d)', msg.eventid, retry, config.amqp.retrynum);
    const resexec = await rd.execStatus(msg.execid);
    if (resexec.status) {
      if (resexec.status == 'running') {
        await channelWrapper.nack(data, false, false);
        log.warn('Failed to process message! EventID %d (%d/%d) Warning: ExecutionID %d is still running', msg.eventid, retry, config.amqp.retrynum, msg.execid);
      } else {
        msg.execstatus = resexec.status;
        msg.execduration = (resexec['date-ended'].unixtime - resexec['date-started'].unixtime)/1000;
        const reslog = await rd.execLog(msg.execid);
        msg.message = new Buffer.from(`Rundeck job execution ${msg.execstatus} in ${msg.execduration} seconds.\n\nJob log:\n\n${reslog}`).toString('base64');
        await pool.query(`UPDATE ${config.db.tbevt} SET execstatus = ?, execduration = ? WHERE eventid = ?`, [msg.execstatus, msg.execduration, msg.eventid]);
        await channelWrapper.publish(config.amqp.exinput, queueout, new Buffer.from(JSON.stringify(msg)), config.amqp.msgopt);
        await channelWrapper.ack(data);
        log.info('Processed message. EventID %d ExecutionID %d (%d/%d)', msg.eventid, msg.execid, retry, config.amqp.retrynum);
      }
    } else {
      await channelWrapper.nack(data, false, false);
      log.warn('Failed to process message! EventID %d (%d/%d) Error: Got error response from server, response:', msg.eventid, retry, config.amqp.retrynum, resexec);
    }
  } catch (err) {
    await channelWrapper.nack(data, false, false);
    log.warn('Failed to process message! EventID %d (%d/%d) Error:', msg.eventid, retry, config.amqp.retrynum, err.message);
  }
}

// Setup AMQP loggin
amqp.logging(queuein);
// Start message processing
amqp.consume(processData, queuein, queueout);
