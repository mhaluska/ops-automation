const config = require('./includes/config.js');
const amqp = require('./includes/rabbitmq.js');
const pool = require('./includes/db.js').pool;
const log4js = require('log4js').configure(config.log);

const queuein = 'zabbix.event.problem';
const queueout = 'otrs.ticket.create';

const log = log4js.getLogger(queuein);
const channelWrapper = amqp.channelWrapper;

// Main processing function
const processData = async (data, retry) => {
  const msg = JSON.parse(data.content);
  try {
    log.info('Got new message. EventID %d (%d/%d)', msg.eventid, retry, config.amqp.retrynum);
    await pool.query(`INSERT INTO ${config.db.tbevt} (
      eventid, eventtime, hostname, triggername
      ) VALUES (
      ?, ?, ?, ?
      )`, [msg.eventid, msg.eventtime, msg.hostname, msg.trigger]);
    await channelWrapper.publish(config.amqp.exinput, queueout, new Buffer.from(data.content), config.amqp.msgopt);
    await channelWrapper.ack(data);
    log.info('Processed message. EventID %d (%d/%d)', msg.eventid, retry, config.amqp.retrynum);
  } catch (err) {
    await channelWrapper.nack(data, false, false);
    log.warn('Failed to process message! EventID %d (%d/%d) Error:', msg.eventid, retry, config.amqp.retrynum, err.message);
  }
}

// Setup AMQP logging
amqp.logging(queuein);
// Start message processing
amqp.consume(processData, queuein, queueout);
