// init config
let config = {};
config.amqp = {};
config.db = {};
config.rundeck = {};
config.zabbix = {};
config.otrs = {};

// START EDIT HERE
// define project var
config.project = 'project01';

// define logging
config.log = {
  appenders: {
    stdout: { type: 'stdout', layout: { type: 'pattern', pattern: '[%d] %[[%p]%] [%z] [%c] %m' }},
    file: { type: 'file', filename: `/tmp/${config.project}.log`, layout: { type: 'pattern', pattern: '[%d] [%p] [%z] [%c] %m' }}
  },
  categories: {
    default: { appenders: [ 'stdout', 'file' ], level: 'info' }
  }
};

// define RabbitMQ vars
config.amqp.host = '127.0.0.1';
config.amqp.port = 5672;
config.amqp.vhost = config.project;
config.amqp.user = config.project;
config.amqp.passwd = 'project01';
config.amqp.retryms = 30000;  // 30s
config.amqp.retrynum = 20;  // 20*30s --> 10m
config.amqp.failedms = 86400000;  // 1d

// define DB vars
config.db.host = '127.0.0.1';
config.db.port = '3306';
config.db.user = config.project;
config.db.passwd = 'project01';
config.db.database = 'automation';
config.db.connlimit = 3;
config.db.client = '%';

// define Rundeck vars
config.rundeck.url = 'https://rundeck.example.com';
config.rundeck.api = 27;
config.rundeck.token = 'RundeckAccessToken';

// define Zabbix vars
config.zabbix.url = 'https://zabbix.example.com/api_jsonrpc.php';
config.zabbix.user = config.project;
config.zabbix.passwd = 'project01';

// define OTRS vars
config.otrs.url = 'https://otrs.example.com/otrs';
config.otrs.restname = 'REST';
config.otrs.user = 'project01@project01.local';
config.otrs.passwd = 'project01';
config.otrs.queue = 'Project01';
config.otrs.tickettype = 'Unclassified';
// STOP EDIT HERE

// DO NOT EDIT HERE
config.amqp.connect = `amqp://${config.amqp.user}:${config.amqp.passwd}@${config.amqp.host}:${config.amqp.port}/${config.amqp.vhost}`;
config.amqp.exinput = `${config.project}.input`;
config.amqp.exretry = `${config.project}.retry`;
config.amqp.exfailed = `${config.project}.failed`;
config.amqp.qretry = `${config.project}.retry`;
config.amqp.qfailed = `${config.project}.failed`;
config.amqp.qsetup = {durable: true, arguments: {'x-dead-letter-exchange': config.amqp.exretry}}
config.amqp.msgopt = { contentType: 'application/json', persistent: true, mandatory: true };

config.db.version = 1;
config.db.tbevt = `${config.project}_events`;
config.db.tbver = `${config.project}_version`;

// export config
module.exports = config;
