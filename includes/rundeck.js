const config = require('./config.js');
const fetch = require('node-fetch');

exports.getJobID = async (project, job) => {
  try {
    const url = `${config.rundeck.url}/api/${config.rundeck.api}/project/${project}/jobs?jobExactFilter=${job}&authtoken=${config.rundeck.token}&format=json`;
    const opt = { method: 'GET', headers: { 'Content-Type': 'application/json' }};

    const res = await fetch(url, opt);
    const json = await res.json();
    return json;
  } catch (err) {
    return err;
  }
}

exports.runJobID = async (jobid, hostname, item) => {
  try {
    const rdopt = {
      options: {
        hostname: hostname,
        item: item
        }
    }
    const url = `${config.rundeck.url}/api/${config.rundeck.api}/job/${jobid}/executions?authtoken=${config.rundeck.token}&format=json`;
    const opt = { method: 'POST', body: JSON.stringify(rdopt), headers: { 'Content-Type': 'application/json' }};

    const res = await fetch(url, opt);
    const json = await res.json();
    return json;
  } catch (err) {
    return err;
  }
}

exports.execStatus = async (execid) => {
  try {
    const url = `${config.rundeck.url}/api/${config.rundeck.api}/execution/${execid}?authtoken=${config.rundeck.token}&format=json`;
    const opt = { method: 'GET', headers: { 'Content-Type': 'application/json' }};

    const res = await fetch(url, opt);
    const json = await res.json();
    return json;
  } catch (err) {
    return err;
  }
}

exports.execLog = async (execid) => {
  try {
    const url = `${config.rundeck.url}/api/${config.rundeck.api}/execution/${execid}/output?authtoken=${config.rundeck.token}&format=text`;
    const opt = { method: 'GET', headers: { 'Content-Type': 'text/plain' }};

    const res = await fetch(url, opt);
    const text = await res.text();
    return text;
  } catch (err) {
    return err;
  }
}
