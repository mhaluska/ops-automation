const config = require('./config.js');
const fetch = require('node-fetch');

exports.createTicket = async (title, sev, body) => {
  try {
    // Create ticket object
    const create = new Object();
    create.Ticket = new Object();
    create.Article = new Object();
    create.Ticket.Type = config.otrs.tickettype;
    create.Ticket.Queue = config.otrs.queue;
    create.Ticket.State = 'new';
    create.Ticket.CustomerUser = config.otrs.user;
    create.Article.Subject = 'Automation create ticket';
    create.Article.ContentType = 'text/plain; charset=utf8';

    create.Ticket.Title = title;
    create.Ticket.PriorityID = sev;
    create.Article.Body = body;

    const url = `${config.otrs.url}/nph-genericinterface.pl/Webservice/${config.otrs.restname}/Ticket?CustomerUserLogin=${config.otrs.user}&Password=${config.otrs.passwd}`;
    const opt = { method: 'POST', body: JSON.stringify(create), headers: { 'Content-Type': 'application/json' }};

    const res = await fetch(url, opt);
    const json = await res.json();
    return json;
  } catch (err) {
    return err;
  }
}

exports.updateTicket = async (ticketid, state, subject, body) => {
  try {
    // Update ticket object
    const update = new Object();
    update.Ticket = new Object();
    update.Article = new Object();
    update.Ticket.CustomerUser = config.otrs.user;
    update.Article.ContentType = 'text/plain; charset=utf8';

    update.Ticket.State = state;
    update.Article.Subject = subject;
    update.Article.Body = body;

    const url = `${config.otrs.url}/nph-genericinterface.pl/Webservice/${config.otrs.restname}/Ticket/${ticketid}?CustomerUserLogin=${config.otrs.user}&Password=${config.otrs.passwd}`;
    const opt = { method: 'PATCH', body: JSON.stringify(update), headers: { 'Content-Type': 'application/json' }};

    const res = await fetch(url, opt);
    const json = await res.json();
    if (json.TicketID) {
      return 'OK';
    } else {
      return 'Error: ' + json;
    }
  } catch (err) {
    return err;
  }
}
