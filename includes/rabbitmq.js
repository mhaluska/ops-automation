const config = require('./config.js');
const amqp = require('amqp-connection-manager');
const log4js = require('log4js').configure(config.log);

// Create a connetion manager
const connection = amqp.connect([config.amqp.connect]);

// Set up default channel
const channelWrapper = connection.createChannel({
  setup: channel =>
    // `channel` here is a regular amqplib `ConfirmChannel`.
    Promise.all([
      channel.assertExchange(config.amqp.exretry, 'topic', {durable: true}),
      channel.assertExchange(config.amqp.exfailed, 'topic', {durable: true}),
      channel.assertExchange(config.amqp.exinput, 'direct', {durable: true}),
      channel.assertQueue(config.amqp.qfailed, {durable: true, arguments: {'x-queue-mode': 'lazy', 'x-message-ttl': config.amqp.failedms}}),
      channel.assertQueue(config.amqp.qretry, {durable: true, arguments: {'x-dead-letter-exchange': config.amqp.exinput, 'x-message-ttl': config.amqp.retryms}}),
      channel.bindQueue(config.amqp.qfailed, config.amqp.exfailed, '#'),
      channel.bindQueue(config.amqp.qretry, config.amqp.exretry, '#'),
      channel.prefetch(1)
    ])
});

//exports.connection = connection;
exports.channelWrapper = channelWrapper;

// Setup logging
exports.logging = function(queuein) {
  if (!queuein) throw 'Variable "queuein" is required! Help <exports>.logging(queuein)';
  const log = log4js.getLogger(queuein);
  connection.on('connect', () => log.info('Connected to RabbitMQ server.'));
  connection.on('disconnect', params => log.error('RabbitMQ server disconnected, error:', params.err.message));
  channelWrapper.on('error', params => log.error('RabbitMQ channel error:', params.err.message));
}

// Setup & consume default function
exports.consume = function(processData, queuein, queueout1, queueout2) {

  if (!processData || !queuein) {
    throw 'Process data function "processData" and "queuein" is required! Help: <exports>.consume(consumeData, queuein, queueout1, queueout2)';
  }
  const log = log4js.getLogger(queuein);

  // Processing function for max retries reached
  const failedMsg = async data => {
    try {
      const msg = JSON.parse(data.content);
      await channelWrapper.publish(config.amqp.exfailed, queuein, new Buffer.from(data.content), { contentType: 'application/json', persistent: true });
      await channelWrapper.ack(data);
      log.error('Failed to process message! EventID %d Error: Max retries reached, message sent to %s', msg.eventid, config.amqp.exfailed);
    } catch (err) {
      await channelWrapper.nack(data, false, false);
      log.error('Failed to process message! EventID %d Error:', msg.eventid, err.message);
    }
  }

  // Processing function with defined retries
  const consumeData = data => {
    if (data.properties.headers) {
      var xdeath = data.properties.headers['x-death'];
    }
    if (!xdeath) {
      var retry = 0;
      processData(data, retry);
    } else if (xdeath && xdeath[0].count <= config.amqp.retrynum) {
      var retry = xdeath[0].count;
      processData(data, retry);
    } else {
      failedMsg(data);
    }
  }

  // Start consuming
  channelWrapper.addSetup(function(channel) {
    if (queueout2) {
      return Promise.all([
        channel.assertQueue(queuein, config.amqp.qsetup),
        channel.assertQueue(queueout1, config.amqp.qsetup),
        channel.assertQueue(queueout2, config.amqp.qsetup),
        channel.bindQueue(queuein, config.amqp.exinput, queuein),
        channel.bindQueue(queueout1, config.amqp.exinput, queueout1),
        channel.bindQueue(queueout2, config.amqp.exinput, queueout1),
        channel.consume(queuein, consumeData)
    ])
    } else if (queueout1) {
      return Promise.all([
        channel.assertQueue(queuein, config.amqp.qsetup),
        channel.assertQueue(queueout1, config.amqp.qsetup),
        channel.bindQueue(queuein, config.amqp.exinput, queuein),
        channel.bindQueue(queueout1, config.amqp.exinput, queueout1),
        channel.consume(queuein, consumeData)
    ])
    } else {
      return Promise.all([
        channel.assertQueue(queuein, config.amqp.qsetup),
        channel.bindQueue(queuein, config.amqp.exinput, queuein),
        channel.consume(queuein, consumeData)
      ])
    }
  });
}
