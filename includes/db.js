const config = require('./config.js');
const mariadb = require('mariadb');

const dbconn = {
	connectionLimit: config.db.connlimit,
	host: config.db.host,
	user: config.db.user,
	password: config.db.passwd,
	database: config.db.database,
	debug: false,
	waitForConnections: true,
	multipleStatements: false
};

const pool = mariadb.createPool(dbconn);
exports.pool = pool;
