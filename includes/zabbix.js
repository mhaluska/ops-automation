const config = require('./config.js');
const fetch = require('node-fetch');

const zbxin = {
  jsonrpc: '2.0',
  method: 'user.login',
  params: {
    user: config.zabbix.user,
    password: config.zabbix.passwd
  },
  id: 1
}

exports.ack = async (eventid, ticketid) => {
  try {
    const optin = { method: 'POST', body: JSON.stringify(zbxin), headers: { 'Content-Type': 'application/json-rpc' }};
    const resin = await fetch(config.zabbix.url, optin);
    const objin = await resin.json();
    if (objin.result) {
      const zbxack = {
        jsonrpc: '2.0',
        method: 'event.acknowledge',
        params: {
          eventids: eventid,
          action: 6,
          message: 'TicketID: ' + ticketid
        },
        id: 1,
        auth: objin.result
      }
      const optack = { method: 'POST', body: JSON.stringify(zbxack), headers: { 'Content-Type': 'application/json-rpc' }};
      const resack = await fetch(config.zabbix.url, optack);
      const objack = await resack.json();
      if (objack.result) {
        return 'OK';
      } else {
        return 'Error: ' + objack.error.data;
      }
      const zbxout = {
        jsonrpc: '2.0',
        method: 'user.logout',
        params: [],
        id: 1,
        auth: objin.result
      }
      const optout = { method: 'POST', body: JSON.stringify(zbxout), headers: { 'Content-Type': 'application/json-rpc' }};
      await fetch(config.zabbix.url, optout);
    } else {
      return 'Error: ' + objin.error.data;
    }
  } catch (err) {
    return 'Error: ' + err;
  }
}
