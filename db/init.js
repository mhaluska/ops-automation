const config = require('../includes/config.js');
const mariadb = require('mariadb');

// User with DB root access
const adminusr = 'admin';
const adminpw = 'admin';
// Remove database
const rmdb = false;
// Remove tables
const rmtb = false;

const pool = mariadb.createPool({
  host: config.db.host,
  port: config.db.port,
  user: adminusr,
  password: adminpw,
  connectionLimit: 1,
  debug: false
});

async function initDb() {
  try {
    if (rmdb === true) await pool.query(`DROP DATABASE IF EXISTS ${config.db.database}`);
    await pool.query(`CREATE DATABASE IF NOT EXISTS ${config.db.database} CHARSET utf8mb4 COLLATE utf8mb4_general_ci`);
    if (rmtb === true) {
      await pool.query(`DROP TABLE IF EXISTS ${config.db.database}.${config.db.tbevt}`);
      await pool.query(`DROP TABLE IF EXISTS ${config.db.database}.${config.db.tbver}`);
    }
    await pool.query(`CREATE TABLE ${config.db.database}.${config.db.tbevt} (
      eventid BIGINT(20) UNSIGNED NOT NULL,
      recoveryid BIGINT(20) UNSIGNED DEFAULT NULL,
      eventtime DATETIME NOT NULL,
      recoverytime DATETIME DEFAULT NULL,
      ticketid BIGINT(20) UNSIGNED DEFAULT NULL,
      ticketnum BIGINT(20) UNSIGNED DEFAULT NULL,
      hostname VARCHAR(255) NOT NULL,
      triggername VARCHAR(255) NOT NULL,
      execid BIGINT(20) UNSIGNED DEFAULT NULL,
      execstatus VARCHAR(255) DEFAULT NULL,
      execduration INT(10) UNSIGNED DEFAULT NULL,
      PRIMARY KEY (eventid),
      UNIQUE KEY id_unique (recoveryid,ticketid,ticketnum,execid)
    )`);
    await pool.query(`CREATE TABLE ${config.db.database}.${config.db.tbver} (version SMALLINT UNSIGNED)`);
    await pool.query(`GRANT ALL ON ${config.db.database}.${config.db.tbevt} TO '${config.db.user}'@'${config.db.client}' IDENTIFIED BY '${config.db.passwd}'`);
    await pool.query(`GRANT ALL ON ${config.db.database}.${config.db.tbver} TO '${config.db.user}'@'${config.db.client}' IDENTIFIED BY '${config.db.passwd}'`);
    await pool.query(`INSERT INTO ${config.db.database}.${config.db.tbver} VALUE (${config.db.version})`);
    await console.log('\x1b[32mDatabase & tables has been successfully created.\x1b[0m');
    process.exit(0);
  } catch (err) {
    await console.log('\x1b[33m' + err.message + '\x1b[0m');
    process.exit(1);
  }
}

initDb();
