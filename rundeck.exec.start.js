const config = require('./includes/config.js');
const amqp = require('./includes/rabbitmq.js');
const pool = require('./includes/db.js').pool;
const rd = require('./includes/rundeck.js');
const log4js = require('log4js').configure(config.log);

const queuein = 'rundeck.exec.start';
const queueout = 'otrs.ticket.update';

const log = log4js.getLogger(queuein);
const channelWrapper = amqp.channelWrapper;

// Main processing function
const processData = async (data, retry) => {
  const msg = JSON.parse(data.content);
  try {
    log.info('Got new message. EventID %d (%d/%d)', msg.eventid, retry, config.amqp.retrynum);
    const resjob = await rd.getJobID(config.project, msg.trigger);

    if (resjob.length == 1 && resjob[0].id) {
      const jobid = resjob[0].id;
      const resrun = await rd.runJobID(jobid, msg.hostname, msg.item);
      if (resrun.id) {
        msg.execstatus = 'started';
        msg.execid = resrun.id;
        msg.message = new Buffer.from(`Rundeck job ${msg.trigger} has been started.\n\nExecution link:\n${resrun.permalink}`).toString('base64');
        await pool.query(`UPDATE ${config.db.tbevt} SET execid = ?, execstatus = ? WHERE eventid = ?`, [msg.execid, msg.execstatus, msg.eventid]);
        await channelWrapper.publish(config.amqp.exinput, queueout, new Buffer.from(JSON.stringify(msg)), config.amqp.msgopt);
        await channelWrapper.ack(data);
        log.info('Processed message. EventID %d ExecutionID %d (%d/%d)', msg.eventid, msg.execid, retry, config.amqp.retrynum);
      } else {
        msg.execstatus = 'notstarted';
        if (typeof resrun == 'object') {
          msg.message = new Buffer.from(`Rundeck job ${msg.trigger} failed to start!\n\nServer response:\n\n${JSON.stringify(resrun)}`).toString('base64');
        } else {
          msg.message = new Buffer.from(`Rundeck job ${msg.trigger} failed to start!\n\nServer response:\n\n${resrun}`).toString('base64');
        }
        await pool.query(`UPDATE ${config.db.tbevt} SET execstatus = ? WHERE eventid = ?`, [msg.execstatus, msg.eventid]);
        await channelWrapper.publish(config.amqp.exfailed, queueout, new Buffer.from(JSON.stringify(msg)), config.amqp.msgopt);
        await channelWrapper.ack(data);
        log.error('Failed to process message! EventID %d (%d/%d) Error: Failed to start JobID %s, response:', msg.eventid, retry, config.amqp.retrynum, jobid, resrun);
      }
    } else {
      if (resjob.length == 0) {
        msg.execstatus = 'notstarted';
        msg.message = new Buffer.from(`Rundeck job ${msg.trigger} not found!`).toString('base64');
        await pool.query(`UPDATE ${config.db.tbevt} SET execstatus = ? WHERE eventid = ?`, [msg.execstatus, msg.eventid]);
        await channelWrapper.publish(config.amqp.exinput, queueout, new Buffer.from(JSON.stringify(msg)), config.amqp.msgopt);
        await channelWrapper.ack(data);
        log.error('Failed to process message! EventID %d (%d/%d) Error: Job %s not found', msg.eventid, retry, config.amqp.retrynum, msg.trigger);
      } else {
        if (resjob[1].id) {
          msg.execstatus = 'notstarted';
          var res = 'Jobs ID found: ';
          await resjob.forEach(function(job) { res += job.id + ' ' });
          msg.message = new Buffer.from(`Rundeck job ${msg.trigger} matched more results!\n\n${res}`).toString('base64');
          await pool.query(`UPDATE ${config.db.tbevt} SET execstatus = ? WHERE eventid = ?`, [msg.execstatus, msg.eventid]);
          await channelWrapper.publish(config.amqp.exinput, queueout, new Buffer.from(JSON.stringify(msg)), config.amqp.msgopt);
          await channelWrapper.ack(data);
          log.error('Failed to process message! EventID %d (%d/%d) Error: Job %s matched more results, response:', msg.eventid, retry, config.amqp.retrynum, msg.trigger, res);
        } else {
          await pool.query(`UPDATE ${config.db.tbevt} SET execstatus = ? WHERE eventid = ?`, ['notstarted', msg.eventid]);
          await channelWrapper.nack(data, false, false);
          log.warn('Failed to process message! EventID %d (%d/%d) Error: Got error response from server, response:', msg.eventid, retry, config.amqp.retrynum, resjob);
        }
      }
    }
  } catch (err) {
    await channelWrapper.nack(data, false, false);
    log.warn('Failed to process message! EventID %d (%d/%d) Error:', msg.eventid, retry, config.amqp.retrynum, err.message);
  }
}

// Setup AMQP loggin
amqp.logging(queuein);
// Start message processing
amqp.consume(processData, queuein, queueout);
