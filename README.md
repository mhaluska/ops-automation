[![Join the chat at https://gitter.im/marek-haluska/ops-automation](https://badges.gitter.im/marek-haluska/ops-automation.svg)](https://gitter.im/marek-haluska/ops-automation?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge) [![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.me/marekhaluska)
# Ops Automation: Zabbix, Rundeck, OTRS and RabbitMQ
Ops Automation using Zabbix (monitoring & inventory), Rundeck (execution engine), OTRS (ticketing system) and RabbitMQ (communication between systems). Also suitable for bigger environments with high availability requirement.

- [Event flow](README.md#event-flow)
- [Prerequisites & versions](README.md#prerequisites-versions)
- [Internal database](README.md#internal-database)
- [Setup](README.md#setup)
  - [RabbitMQ](README.md#rabbitmq)
  - [OTRS](README.md#otrs)
  - [Rundeck](README.md#rundeck)
  - [Zabbix](README.md#zabbix)
  - [JavaScript workers](README.md#javascript-workers)
 - [Testing](README.md#testing)

## Event flow

1. Zabbix PROBLEM trigger is activated &rarr; sendevent.js &rarr; queue zabbix.event.problem
2. Consumer zabbix.event.problem &rarr; db insert &rarr; queue otrs.ticket.create
3. Consumer otrs.ticket.create &rarr; create ticket & db update &rarr; queue rundeck.exec.start & zabbix.event.ack
4. Consumer rundeck.exec.start &rarr; start job (if found) & db update &rarr; queue otrs.ticket.update
5. Consumer queue otrs.ticket.update &rarr; update ticket &rarr; queue rundeck.exec.watch (if execution has been started)
6. Consumer rundeck.exec.watch &rarr; db update &rarr; queue otrs.ticket.update
7. Consumer otrs.ticket.update &rarr; update ticket &rarr; done
8. Zabbix OK trigger is activated &rarr; sendevent.js &rarr; queue zabbix.event.ok
9. Consumer zabbix.event.ok &rarr; get ticket id from db &rarr; otrs.ticket.resolve
10. Consumer otrs.ticket.resolve &rarr; update/resolve ticket (based on execution status) &rarr; done

## Prerequisites & versions

### Prerequisites

1. [Zabbix](https://www.zabbix.com/)
2. [Rundeck](https://www.rundeck.com/open-source)
3. [OTRS](https://community.otrs.com/)
4. [MariaDB](https://mariadb.org/) or [MySQL](https://www.mysql.com/)
5. [RabbitMQ](https://www.rabbitmq.com/)
6. [Node.js](https://nodejs.org/)

### Versions tested

- OS: CentOS 7.5.1804
- Zabbix: 4.0.1
- Rundeck: 3.0.8
- OTRS: 6.0.12
- MariaDB: 10.2.17
- RabbitMQ: 3.7.8
- Node.js: 10.13.0

## Internal database

Internal database is mainly used for getting OTRS TicketID and Rundeck job status for OK events. Eventually can be used for reporting.

Data in database:
- Zabbix PROBLEM event {EVENT.ID} `eventid`
- Zabbix OK event {EVENT.RECOVERY.ID} `recoveryid`
- Zabbix PROBLEM event {EVENT.DATE} {EVENT.TIME} `eventtime`
- Zabbix OK event {EVENT.RECOVERY.DATE} {EVENT.RECOVERY.TIME} `recoverytime`
- OTRS ticket TicketID `ticketid`
- OTRS ticket Ticket# `ticketnum`
- Zabbix event {HOST.NAME} `hostname`
- Zabbix event {TRIGGER.NAME} `triggername`
- Rundeck execution id `execid`
- Rundeck execution status `execstatus`
- Rundeck execution duration `execduration`

## Setup

Default password in configuration file [includes/config.js](includes/config.js) is `project01`.
```
config.amqp.passwd = 'project01';
config.db.passwd = 'project01';
config.zabbix.passwd = 'project01';
config.otrs.passwd = 'project01';
```

### RabbitMQ

1. Create virtual host: `project01`
2. Create user: `project01`
3. Grant full access for user to virtual host

**Consumer failed processing message retry and delay detail.**
1. If message processing fails, consumer sends basic.nack &rarr; retry exchange &rarr; retry queue with x-message-ttl (delay).
2. When x-message-ttl expires &rarr; input exchange &rarr; original queue.
3. Consumer receives original message with x-death header and check x-death[0].count (current retry).
4. When consumer receives message with retry > config.amqp.retrynum (max retry), then sends basic.ack and message to failed exchange &rarr; failed queue.
5. Failed queue has also x-message-ttl, but without x-dead-letter-exchange, so expired messages are discarded.

Retry and delay configuration in [includes/config.js](includes/config.js):
```
config.amqp.retryms = 30000;  // 30s
config.amqp.retrynum = 20;  // 20*30s --> 10m
config.amqp.failedms = 86400000;  // 1d
```

### OTRS

1. Create queue: `Project01`
2. Create customer:
   - Customer ID: `project01`
   - Customer name: `Project01`
3. Create customer user: `project01@project01.local`
4. Grant customer user access to queue
5. Import Web Service from following file and set name to `REST`: [REST.yml](setup/otrs/REST.yml)

### Rundeck

1. Copy files [setup/rundeck/nodes.*](setup/rundeck/) to `/var/rundeck`
2. Update variables `HOST`, `USER`, `PASSWD` in script `nodes.sh`
3. Create project `project01`
4. Add script node source, format: `resourcexml`, script path `/var/rundeck/scripts/automation/nodes.sh`, arguments: `Project01`
5. Import example job [Automation-FileExist-Example.xml](setup/rundeck/Automation-FileExist-Example.xml)

### Zabbix

#### Alert handler script

1. Copy script [sendevent.js](setup/zabbix/sendevent.js) to `/usr/lib/zabbix/alertscripts`
2. Update script variables:
   ```
   const modpath = '/usr/lib/node_modules';
   const logpath = '/var/log/zabbix/event2amqp.log';
   const amqpurl = 'amqp://project01:project01@127.0.0.1:5672/project01';
   ```

3. Install global node modules: `# npm i -g amqp-connection-manager amqplib log4js`

#### Zabbix inventory

Required values to fill for every host are `Name`, `Asset tag` and `Installer name`.

- `Name` set to Zabbix agent name
- `Asset tag` set to `Project01`
- `Installer name` set to user with access from Rundeck

Optional values are OS, OS (Full details), OS (short), Tag and HW architecture.

- Example Zabbix inventory  
![Zabbix inventory](https://gitlab.com/mhaluska/zabbix-rundeck-otrs/raw/master/images/zabbix-inventory.png)  
- Example Rundeck inventory  
![Rundeck inventory](https://gitlab.com/mhaluska/zabbix-rundeck-otrs/raw/master/images/rundeck-inventory.png)  

#### Zabbix template

In Configuration &rarr; Template choose import and load [zbx_export_templates.xml](setup/zabbix/zbx_export_templates.xml).  
Following options must be selected in `Create new` section:
- Groups
- Templates
- Applications
- Items
- Triggers

When done, assign host(s) with properly filled inventory data to template `Template Automation Linux`.

#### Zabbix media type and user

1. Create new media type in Administration &rarr; Media types
 - Name: `Ops Automation`
 - Type: `Script`
 - Script name: `sendevent.js`
 - Add 3 parameters, order is important:
   - `{ALERT.SUBJECT}`
   - `{ALERT.MESSAGE}`
   - `{ALERT.SENDTO}`

2. Create new user in Administration &rarr; Users and grant him required access to Zabbix agent(s):
 - Alias: `project01`
 - Password: `project01`
 - In Media tab:
   - Type: `Ops Automation`
   - Send to: `project01`

#### Zabbix action

In Configuration &rarr; Actions add new Action.  

1\. Action:
 - Name: `Ops Automation`
 - Type of calculation: `And`
 - Conditions: `Maintenance status not in maintenance` And `Template = Template Automation Linux`

2\. Operations (strict format):
 - Default subject: `{HOST.NAME}:{TRIGGER.NAME}:{TRIGGER.NSEVERITY}:{EVENT.ID}:{TRIGGER.STATUS}`
 - Default message:

```
Problem started: {EVENT.DATE} {EVENT.TIME}

Problem name: {TRIGGER.NAME}
Host: {HOST.NAME}
Severity: {TRIGGER.SEVERITY}
Trigger status: {TRIGGER.STATUS}

Additional info: {ITEM.KEY1}:{ITEM.VALUE1}

Original problem ID: {EVENT.ID}
```
 - Operations:
   - Operation type `Send message`
   - Send to Users `project01`
   - Send only to `Ops Automation`   
   
3\. Recovery operations (strict format):
 - Default subject: `{HOST.NAME}:{TRIGGER.NAME}:{TRIGGER.NSEVERITY}:{EVENT.ID}:{TRIGGER.STATUS}:{EVENT.RECOVERY.ID}`
 - Default message:

```
Problem resolved: {EVENT.RECOVERY.DATE} {EVENT.RECOVERY.TIME}

Problem name: {TRIGGER.NAME}
Host: {HOST.NAME}
Severity: {TRIGGER.SEVERITY}
Trigger status: {TRIGGER.STATUS}

Additional info: {ITEM.KEY1}:{ITEM.VALUE1}

Original problem ID: {EVENT.ID}
Recovery problem ID: {EVENT.RECOVERY.ID}
```  
 - Operations:
   - Operation type `Send message`
   - Send to Users `project01`
   - Send only to `Ops Automation`  

## JavaScript workers

1. Clone this repository: `$ git clone https://gitlab.com/mhaluska/ops-automation.git && cd ops-automation`
2. Update IPs, URLs and Rundeck token in [includes/config.js](includes/config.js):
   ```
   config.amqp.host = '127.0.0.1';
   config.db.host = '127.0.0.1';
   config.rundeck.url = 'https://rundeck.example.com';
   config.zabbix.url = 'https://zabbix.example.com/api_jsonrpc.php';
   config.otrs.url = 'https://otrs.example.com/otrs';
   config.rundeck.token = 'RundeckAccessToken';
   ```

3. Update [db/init.js](db/init.js) user configuration with CREATE and GRANT privileges:
   ```
   const adminusr = 'admin';
   const adminpw = 'admin';
   ```

4. Setup database and tables for storing automation states: `$ node db/init.js`
5. Start all *.js scripts in current directory: `$ node <script name>.js` or use [PM2](http://pm2.keymetrics.io/)

**You should remove file or admin variables from [db/init.js](db/init.js) when DB & tables are initialized.**

By default logging is enabled to stdout and file /tmp/project01.log

## Testing

1. On host with Automation template assigned, trigger event where trigger name match Rundeck job name:  
   `# rm -f /tmp/file.create`  
   or  
   `# touch /tmp/file.remove`  
2. On host with Automation template assigned, trigger event where trigger name doesn't match Rundeck job name:  
    Create script `killme.sh`  

    ```
    #!/bin/bash
    while true; do
     date
     sleep 30
    done
    ```  
    Run the script `nohup ./killme.sh 2>&1 &`  
3. Now if everything has been configured properly you should see Zabbix events triggering Rundeck jobs and Rundeck jobs creating/updating/resolving tickets on OTRS.