const config = require('./includes/config.js');
const amqp = require('./includes/rabbitmq.js');
const otrs = require('./includes/otrs.js');
const log4js = require('log4js').configure(config.log);

const queuein = 'otrs.ticket.update';
const queueout = 'rundeck.exec.watch';

const log = log4js.getLogger(queuein);
const channelWrapper = amqp.channelWrapper;

// Main processing function
const processData = async (data, retry) => {
  const msg = JSON.parse(data.content);
  try {
    log.info('Got new message. EventID %d (%d/%d)', msg.eventid, retry, config.amqp.retrynum);
    const body = new Buffer.from(msg.message, 'base64').toString('utf8');

    if (!msg.execid) {

      const res = await otrs.updateTicket(msg.ticketid, 'open', 'Automation not executed', body);
      if (res == 'OK') {
        await channelWrapper.ack(data);
        log.info('Processed message. EventID %d TicketID %d (%d/%d)', msg.eventid, msg.ticketid, retry, config.amqp.retrynum);
      } else {
        await channelWrapper.nack(data, false, false);
        log.warn('Failed to process message! EventID %d (%d/%d)', msg.eventid, retry, config.amqp.retrynum, res);
      }

    } else if (msg.execid && msg.execstatus == 'started') {

      const res = await otrs.updateTicket(msg.ticketid, 'open', 'Automation execution started', body);
      if (res == 'OK') {
        await delete msg.message; // Remove message from JSON, no more required
        await channelWrapper.publish(config.amqp.exretry, queueout, new Buffer.from(JSON.stringify(msg)), config.amqp.msgopt);
        await channelWrapper.ack(data);
        log.info('Processed message. EventID %d TicketID %d (%d/%d)', msg.eventid, msg.ticketid, retry, config.amqp.retrynum);
      } else {
        await channelWrapper.nack(data, false, false);
        log.warn('Failed to process message! EventID %d (%d/%d)', msg.eventid, retry, config.amqp.retrynum, res);
      }

    } else if (msg.execid && msg.execstatus == 'succeeded') {
      const res = await otrs.updateTicket(msg.ticketid, 'open', 'Automation execution succeeded', body);
      if (res == 'OK') {
        await channelWrapper.ack(data);
        log.info('Processed message. EventID %d TicketID %d (%d/%d)', msg.eventid, msg.ticketid, retry, config.amqp.retrynum);
      } else {
        await channelWrapper.nack(data, false, false);
        log.warn('Failed to process message! EventID %d (%d/%d)', msg.eventid, retry, config.amqp.retrynum, res);
      }

    } else {
      const res = await otrs.updateTicket(msg.ticketid, 'open', 'Automation execution failed', body);
      if (res == 'OK') {
        await channelWrapper.ack(data);
        log.info('Processed message. EventID %d TicketID %d (%d/%d)', msg.eventid, msg.ticketid, retry, config.amqp.retrynum);
      } else {
        await channelWrapper.nack(data, false, false);
        log.warn('Failed to process message! EventID %d (%d/%d)', msg.eventid, retry, config.amqp.retrynum, res);
      }
    }

  } catch (err) {
    await channelWrapper.nack(data, false, false);
    log.warn('Failed to process message! EventID %d (%d/%d) Error:', msg.eventid, retry, config.amqp.retrynum, err.message);
  }
}

// Setup AMQP logging
amqp.logging(queuein);
// Start message processing
amqp.consume(processData, queuein, queueout);
