const config = require('./includes/config.js');
const amqp = require('./includes/rabbitmq.js');
const zabbix = require('./includes/zabbix.js');
const log4js = require('log4js').configure(config.log);

const queuein = 'zabbix.event.ack';

const log = log4js.getLogger(queuein);
const channelWrapper = amqp.channelWrapper;

// Main processing function
const processData = async (data, retry) => {
  const msg = JSON.parse(data.content);
  try {
    log.info('Got new message. EventID %d (%d/%d)', msg.eventid, retry, config.amqp.retrynum);
    const res = await zabbix.ack(msg.eventid, msg.ticketid);
    if (res == 'OK') {
      await channelWrapper.ack(data);
      log.info('Processed message. EventID %d (%d/%d)', msg.eventid, retry, config.amqp.retrynum);
    } else {
      await channelWrapper.nack(data, false, false);
      log.warn('Failed to process message! EventID %d (%d/%d)', msg.eventid, retry, config.amqp.retrynum, res);
    }
  } catch (err) {
    await channelWrapper.nack(data, false, false);
    log.warn('Failed to process message! EventID %d (%d/%d) Error:', msg.eventid, retry, config.amqp.retrynum, err.message);
  }
}

// Setup AMQP logging
amqp.logging(queuein);
// Start message processing
amqp.consume(processData, queuein);
