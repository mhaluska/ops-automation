const config = require('./includes/config.js');
const amqp = require('./includes/rabbitmq.js');
const pool = require('./includes/db.js').pool;
const otrs = require('./includes/otrs.js');
const log4js = require('log4js').configure(config.log);

const queuein = 'otrs.ticket.create';
const queueout = 'rundeck.exec.start';
const zabbixack = 'zabbix.event.ack';

const log = log4js.getLogger(queuein);
const channelWrapper = amqp.channelWrapper;

// Main processing function
const processData = async (data, retry) => {
  const msg = JSON.parse(data.content);
  try {
    log.info('Got new message. EventID %d (%d/%d)', msg.eventid, retry, config.amqp.retrynum);
    const title = `${msg.hostname} - ${msg.trigger} - ${msg.item}`;
    const body = new Buffer.from(msg.message, 'base64').toString('utf8');
    const res = await otrs.createTicket(title, msg.severity, body);
    if (res.TicketID) {
      msg.ticketid = res.TicketID;
      await pool.query(`UPDATE ${config.db.tbevt} SET ticketid = ?, ticketnum = ? WHERE eventid = ?`, [res.TicketID, res.TicketNumber, msg.eventid]);
      await delete msg.message; // Remove message from JSON, no more required
      await channelWrapper.publish(config.amqp.exinput, queueout, new Buffer.from(JSON.stringify(msg)), config.amqp.msgopt);
      await channelWrapper.ack(data);
      log.info('Processed message. EventID %d TicketID %d (%d/%d)', msg.eventid, msg.ticketid, retry, config.amqp.retrynum);
    } else {
      log.warn('Failed to process message! EventID %d (%d/%d) Error response:', msg.eventid, retry, config.amqp.retrynum, res);
      await channelWrapper.nack(data, false, false);
    }
  } catch (err) {
    await channelWrapper.nack(data, false, false);
    log.warn('Failed to process message! EventID %d (%d/%d) Error:', msg.eventid, retry, config.amqp.retrynum, err.message);
  }
}

// Setup AMQP logging
amqp.logging(queuein);
// Start message processing
amqp.consume(processData, queuein, queueout, zabbixack);
