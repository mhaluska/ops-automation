#!/usr/bin/node

// Modules, AMQP and logging configuration
const modpath = '/usr/lib/node_modules';
const logpath = '/var/log/zabbix/event2amqp.log';
const amqpurl = 'amqp://project01:project01@127.0.0.1:5672/project01';

// Parse arguments passed to script
const args = process.argv.slice(2);
const [ subject, message, project ] = args;

// Parse & convert event time
const eventtime = message.match(/^Problem (started|resolved): ([0-9.]+\s[0-9:]+)$/m)[2].replace(/\./g, '-');

// Parse "Additional info", event item definition
const item = message.match(/^Additional info:\s?(.*)$/m)[1];

// Prepare JSON payload
json = new Object();
[ json.hostname, json.trigger, json.severity, json.eventid, json.triggerstatus, json.recoveryid ] = subject.split(':');
json.eventtime = eventtime;
json.item = item;
json.message = new Buffer.from(message).toString('base64');

const payload = new Buffer.from(JSON.stringify(json));

// Configure logging
const log4js = require(`${modpath}/log4js`);
log4js.configure({
  appenders: {
    stderr: { type: 'stderr', layout: { type: 'pattern', pattern: '[%d] %[[%p]%] [%z] [%c] %m' }},
    file: { type: 'file', filename: logpath, layout: { type: 'pattern', pattern: '[%d] [%p] [%z] [%c] %m'}}
  },
  categories: {
    default: { appenders: [ 'stderr', 'file' ], level: 'info' }
  }
});
const log = log4js.getLogger(project);

// Configure MQ
const amqp = require(`${modpath}/amqp-connection-manager`);
const connection = amqp.connect([amqpurl]);

const exchange = `${project}.input`;
const exretry = `${project}.retry`;
const queue = `zabbix.event.${json.triggerstatus.toLowerCase()}`;

var channelWrapper = connection.createChannel({
  setup: channel =>
    Promise.all([
      channel.assertExchange(exchange, 'direct', {durable: true}),
      channel.assertQueue(queue, {durable: true, arguments: {'x-dead-letter-exchange': exretry}}),
      channel.bindQueue(queue, exchange, queue),
      channel.on('error', (err) => {
        log.error('RabbitMQ channel error:', err.message);
        process.exit(1);
      })
    ])
});

// Send event function
var sendPayload = async function() {
  try {
    await channelWrapper.publish(exchange, queue, payload, { contentType: 'application/json', persistent: true, mandatory: true });
    await log.info('Event published. EventStatus: %s, EventID: %d, Hostname: %s, Trigger: %s.', json.triggerstatus, json.eventid, json.hostname, json.trigger);
    connection.close();
    process.exit(0);
  } catch (err) {
    await log.error('Event publish failed! EventStatus: %s, EventID: %d, Hostname: %s, Trigger: %s. Error:', json.triggerstatus, json.eventid, json.hostname, json.trigger, err.message);
    connection.close();
    process.exit(1);
  }
};

sendPayload();
