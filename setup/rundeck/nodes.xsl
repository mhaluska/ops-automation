<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <xsl:output indent="yes" encoding="UTF-8"/>
  <xsl:strip-space elements="*"/>

  <xsl:template match="resultset">
    <project>
      <xsl:apply-templates/>
    </project>
  </xsl:template>

  <xsl:template match="row">
    <node name="{field[@name='name']}"
        hostname="{field[@name='ip']}"
        tags="{field[@name='tag']}"
	osArch="{field[@name='hw_arch']}"
	osFamily="{field[@name='os_short']}"
	osName="{field[@name='os']}"
	osVersion="{field[@name='os_full']}"
	username="{field[@name='installer_name']}"
    />
  </xsl:template>

</xsl:stylesheet>
