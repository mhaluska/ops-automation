#!/bin/bash

# Zabbix database connection
HOST=127.0.0.1
DB=zabbix
USER=zabbix
PASSWD=zabbix
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

PROJECT=$1

mysql -h$HOST -u$USER -p$PASSWD -X -e \
	"SELECT name,os,os_full,os_short,tag,hw_arch,installer_name,ip
	FROM host_inventory,interface
	WHERE host_inventory.hostid=interface.hostid AND asset_tag = \"$PROJECT\"
	GROUP BY interface.hostid" \
$DB | xsltproc $SCRIPTPATH/nodes.xsl -
